package Model;

import java.util.ArrayList;
import java.util.List;

public class HomeChristmasDecor extends ChristmasDecorations {
private static List<ChristmasDecorations> list = new ArrayList<>();

    public HomeChristmasDecor() {
    }

    public HomeChristmasDecor(String color, String shape, int cost) {
        super(color, shape, cost);
    }
}
