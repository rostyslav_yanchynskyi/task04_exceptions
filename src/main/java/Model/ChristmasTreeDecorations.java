package Model;

import java.util.ArrayList;
import java.util.List;

public class ChristmasTreeDecorations extends ChristmasDecorations {
private static List<ChristmasDecorations> list = new ArrayList<>();

    public ChristmasTreeDecorations() {
        super();
    }

    public ChristmasTreeDecorations(String color, String shape, int cost) {
        super(color, shape, cost);
    }
}
