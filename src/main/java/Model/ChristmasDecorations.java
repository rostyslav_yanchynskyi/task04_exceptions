package Model;

import java.util.*;

public abstract class ChristmasDecorations {
    String color;
    String shape;
    int cost;

    public ChristmasDecorations(String color, String shape, int cost) {
        this.color = color;
        this.shape = shape;
        this.cost = cost;
    }

    public ChristmasDecorations() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChristmasDecorations that = (ChristmasDecorations) o;
        return cost == that.cost &&
                Objects.equals(color, that.color) &&
                Objects.equals(shape, that.shape);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, shape, cost);
    }

    @Override
    public String toString() {
        return "christmasDecorations{" +
                "color='" + color + '\'' +
                ", shape='" + shape + '\'' +
                ", cost=" + cost +
                '}';
    }
}
