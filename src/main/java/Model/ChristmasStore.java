package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ChristmasStore {
public static List<ChristmasDecorations> homeList = new ArrayList<>();
public static List<ChristmasDecorations> treeList = new ArrayList<>();

    static{
        treeList.add(new ChristmasTreeDecorations("Green","Circle",25));
        homeList.add(new HomeChristmasDecor("Red","Squad",50));
        homeList.add(new HomeChristmasDecor("Pink","Circle",20));
        treeList.add(new ChristmasTreeDecorations("Green","Circle",40));
        treeList.add(new ChristmasTreeDecorations("White","Stick",210));
    }

    public static void printTreeList() {
        for (ChristmasDecorations decorations : treeList) {
            System.out.println(decorations);
        }
    }

    public static void printHomeList() {
        for (ChristmasDecorations decorations : homeList) {
            System.out.println(decorations);
        }
    }

    public static void printAll() {
        printHomeList();
        printTreeList();
    }

    public static void findDecorationByType () throws MyOwnExseption {
        Scanner scr = new Scanner(System.in);
        String decorationType = scr.nextLine();

            if (decorationType.equals("Christmas tree")) {
                printTreeList();
            }else if (decorationType.equals("Home decor")) {
                printHomeList();
            }else {
                throw new MyOwnExseption("Напишіть Christmas tree або Home decor");
            }


    }
}
