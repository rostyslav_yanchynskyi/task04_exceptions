package Controller;

import Model.MyOwnExseption;

public interface Controller {

    void findDecorationByType() throws MyOwnExseption;
    void printAll();
}
