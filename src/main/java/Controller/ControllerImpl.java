package Controller;

import Model.ChristmasStore;
import Model.MyOwnExseption;

public class ControllerImpl implements Controller {

    @Override
    public void findDecorationByType() throws MyOwnExseption {
        ChristmasStore.findDecorationByType();
    }

    @Override
    public void printAll() {
        ChristmasStore.printAll();
    }
}
