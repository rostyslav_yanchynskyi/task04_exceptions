package View;

import Controller.Controller;
import Controller.ControllerImpl;
import Model.MyOwnExseption;

import java.util.Scanner;

public class MyView {
static Controller controller;

    public static void show() throws MyOwnExseption {
        controller = new ControllerImpl();
        boolean isTrue = true;
        do {
            System.out.println("Привіт! Це новорічний магазин." +
                    "\n" + "1.Показати весь товар." +
                    "\n" + "2.Знайти товар за категорією.");
            Scanner scr = new Scanner(System.in);
            int n = scr.nextInt();
            //----------------------
            if (n == 1) {
                controller.printAll();
                isTrue = isTrue(isTrue, scr);
            }
            //-----------------------
            if (n == 2) {
                System.out.println("Введіть тип товару!");
                controller.findDecorationByType();
                isTrue = isTrue(isTrue, scr);
            }
        }while (isTrue);
        }

    private static boolean isTrue(boolean isTrue, Scanner scr) {
        System.out.println("------------------");
        System.out.println("1.Повернутись в меню" +
                "\n" + "2.Вихід");
        int num1 = scr.nextInt();
        if (num1 == 1){

        }else if (num1 == 2) {
            isTrue = false;
            System.out.println("До побачення!");
        }
        return isTrue;
    }

}

